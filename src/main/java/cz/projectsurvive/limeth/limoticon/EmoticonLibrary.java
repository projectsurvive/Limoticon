package cz.projectsurvive.limeth.limoticon;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;
import java.util.stream.Collectors;

/**
 * @author Limeth
 */
public class EmoticonLibrary
{
	private static final String                          KEY_PACKS     = "library.packs";
	private static final String                          KEY_ALIASES   = "library.aliases";
	private static final String                          KEY_FIRSTCHARACTER     = "firstCharacter";
	private static final int                             DEFAULT_FIRSTCHARACTER = 12800;
	private static final String                          DIRECTORY     = "library";
	private final        List<String>                    packIds       = Lists.newArrayList();
	private final        Map<String, EmoticonPack>       packMap       = Maps.newHashMap();
	private final        List<String>                    emoticonIds   = Lists.newArrayList();
	private final        Map<String, Emoticon>           emoticonMap   = Maps.newHashMap();
	private final        Map<EmoticonPackEntry, Integer> indexMap      = Maps.newHashMap();
	private int firstCharacter;

	private EmoticonLibrary()
	{
	}

	public static File getDirectory()
	{
		return new File(Limoticon.getInstance().getDataFolder(), DIRECTORY);
	}

	public static void prepare(YamlConfiguration yml)
	{
		prepareFiles();
		prepareConfig(yml);
	}

	private static void prepareFiles()
	{
		try
		{
			File dir = getDirectory();

			if(dir.isDirectory())
			{
				File[] files = dir.listFiles();

				if(files != null)
					for(File file : files)
						if(file.isDirectory())
							return;
			}

			File packFile1 = EmoticonPack.getDirectory("red");
			File packFile2 = EmoticonPack.getDirectory("green");

			if(!packFile1.exists())
			{
				packFile1.mkdirs();

				File frown = new File(packFile1, "frown.png");
				File laugh = new File(packFile1, "laugh.png");

				if(!frown.exists())
					frown.createNewFile();
				if(!laugh.exists())
					laugh.createNewFile();
			}


			if(!packFile2.exists())
			{
				packFile2.mkdirs();

				File frown = new File(packFile2, "frown.png");
				File laugh = new File(packFile2, "laugh.png");

				if(!frown.exists())
					frown.createNewFile();
				if(!laugh.exists())
					laugh.createNewFile();
			}
		}
		catch(IOException e)
		{
			System.out.println("An error occurred while preparing the emoticon library.");
			e.printStackTrace();
		}
	}

	private static void prepareConfig(YamlConfiguration yml)
	{
		if(!yml.contains(KEY_PACKS))
		{
			yml.set(KEY_PACKS + ".red." + EmoticonPack.KEY_NAME, "Red Pack");
			yml.set(KEY_PACKS + ".red." + EmoticonPack.KEY_DESCRIPTION, "Emoticon pack containing red smileys");
			yml.set(KEY_PACKS + ".red." + EmoticonPack.KEY_DEFAULT, true);
			yml.set(KEY_PACKS + ".green." + EmoticonPack.KEY_NAME, "Green Pack");
			yml.set(KEY_PACKS + ".green." + EmoticonPack.KEY_DESCRIPTION, "Emoticon pack containing green smileys");
		}

		if(!yml.contains(KEY_ALIASES))
		{
			yml.set(KEY_ALIASES + ".frown", Lists.newArrayList(">:\\(", ">=\\("));
			yml.set(KEY_ALIASES + ".laugh", Lists.newArrayList(":D", "=D"));
		}

		if(!yml.contains(KEY_FIRSTCHARACTER))
			yml.set(KEY_FIRSTCHARACTER, DEFAULT_FIRSTCHARACTER);
	}

	public static EmoticonLibrary load(YamlConfiguration yml)
	{
		EmoticonLibrary library = new EmoticonLibrary();

		library.firstCharacter = yml.getInt(KEY_FIRSTCHARACTER, DEFAULT_FIRSTCHARACTER);
		ConfigurationSection packsSection = yml.getConfigurationSection(KEY_PACKS);

		for(String packSectionName : packsSection.getKeys(false))
		{
			ConfigurationSection packSection = yml.getConfigurationSection(KEY_PACKS + "." + packSectionName);
			EmoticonPack pack = EmoticonPack.load(packSection);
			String packId = pack.getId();

			library.packIds.add(packId);
			library.packMap.put(packId, pack);

			pack.getEmoticonIds()
			    .stream()
			    .filter(emoticonId -> !library.emoticonMap.containsKey(emoticonId))
			    .forEach(emoticonId -> {
				    Emoticon emoticon = new Emoticon(emoticonId);
				    List<Pattern> aliases = yml.getStringList(KEY_ALIASES + "." + emoticonId)
				                               .stream()
				                               .map(string -> {
					                               try
					                               {
						                               return Pattern.compile(string);
					                               }
					                               catch(PatternSyntaxException e)
					                               {
						                               System.out.println("Could not compile '" + emoticonId + "' emoticon pattern: " + string + "\nError: " + e.getLocalizedMessage());
						                               return null;
					                               }
				                               }).filter(Objects::nonNull)
				                                 .collect(Collectors.toList());

				    aliases.forEach(emoticon::addAlias);
				    library.emoticonIds.add(emoticonId);
				    library.emoticonMap.put(emoticonId, emoticon);
			    });

			library.emoticonIds.sort(null);
		}

		return library;
	}

	public boolean indexEmoticons()
	{
		ResourcePackBuilder builder = Limoticon.getInstance().getBuilder();

		try
		{
			builder.clearImages();

			int index = firstCharacter;

			for(EmoticonPack pack : getPacks())
				for(Emoticon emoticon : getEmoticons(pack))
				{
					EmoticonPackEntry entry = new EmoticonPackEntry(pack, emoticon);

					while(!builder.isCharacterDefined(index))
						index++;

					indexMap.put(entry, index);
					index++;
				}

			return true;
		}
		catch(ResourcePackBuilderException e)
		{
			System.out.println("An error occurred while indexing emoticons: " + e.getLocalizedMessage());
			e.printStackTrace();
			return false;
		}
		finally
		{
			builder.clearImages();
		}
	}

	public int getFirstCharacter()
	{
		return firstCharacter;
	}

	public Optional<Integer> getCharacterCode(EmoticonPack pack, Emoticon emoticon)
	{
		EmoticonPackEntry entry = new EmoticonPackEntry(pack, emoticon);

		return getCharacterCode(entry);
	}

	public Optional<Integer> getCharacterCode(EmoticonPackEntry entry)
	{
		return Optional.ofNullable(indexMap.get(entry));
	}

	public Optional<Character> getCharacter(EmoticonPack pack, Emoticon emoticon)
	{
		Optional<Integer> code = getCharacterCode(pack, emoticon);

		return code.isPresent() ? Optional.of((char) (int) code.get()) : Optional.<Character>empty();
	}

	public List<String> getPackIds()
	{
		return Lists.newArrayList(packIds);
	}

	public Map<String, EmoticonPack> getPackMap()
	{
		return Maps.newHashMap(packMap);
	}

	public List<EmoticonPack> getPacks()
	{
		return packIds.stream().map(packMap::get).collect(Collectors.toList());
	}

	public List<String> getEmoticonIds()
	{
		return Lists.newArrayList(emoticonIds);
	}

	public Map<String, Emoticon> getEmoticonMap()
	{
		return Maps.newHashMap(emoticonMap);
	}

	public List<Emoticon> getEmoticons()
	{
		return emoticonIds.stream().map(emoticonMap::get).collect(Collectors.toList());
	}

	public List<Emoticon> getEmoticons(EmoticonPack pack)
	{
		return pack.getEmoticonIds().stream().map(emoticonMap::get).collect(Collectors.toList());
	}

	public List<EmoticonPack> getDefaultPacks()
	{
		return getPacks().stream().filter(EmoticonPack::isDefault).collect(Collectors.toList());
	}
}
