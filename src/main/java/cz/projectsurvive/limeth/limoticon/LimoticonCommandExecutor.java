package cz.projectsurvive.limeth.limoticon;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.List;

/**
 * @author Limeth
 */
public class LimoticonCommandExecutor implements CommandExecutor
{
	public static final String COMMAND_HELP              = "help";
	public static final String COMMAND_RELOAD            = "reload";
	public static final String COMMAND_BUILD             = "build";
	public static final String COMMAND_LIST              = "list";
	public static final String COMMAND_ENABLE            = "enable";
	public static final String COMMAND_DISABLE           = "disable";
	public static final String DESCRIPTION_HELP          = "displays available commands";
	public static final String DESCRIPTION_RELOAD        = "reloads config.yml";
	public static final String DESCRIPTION_BUILD         = "builds the resource pack images";
	public static final String DESCRIPTION_LIST          = "shows the emoticon packs";
	public static final String DESCRIPTION_ENABLE        = "enables an emoticon pack";
	public static final String DESCRIPTION_DISABLE       = "disables an emoticon pack";
	public static final String PERMISSION_PREFIX_COMMAND = "limoticon.command.";
	public static final String PERMISSION_HELP           = PERMISSION_PREFIX_COMMAND + COMMAND_HELP;
	public static final String PERMISSION_RELOAD         = PERMISSION_PREFIX_COMMAND + COMMAND_RELOAD;
	public static final String PERMISSION_BUILD          = PERMISSION_PREFIX_COMMAND + COMMAND_BUILD;
	public static final String PERMISSION_LIST           = PERMISSION_PREFIX_COMMAND + COMMAND_LIST;
	public static final String PERMISSION_ENABLE         = PERMISSION_PREFIX_COMMAND + COMMAND_ENABLE;
	public static final String PERMISSION_DISABLE        = PERMISSION_PREFIX_COMMAND + COMMAND_DISABLE;

	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
	{
		if(args.length <= 0)
		{
			sender.sendMessage(ChatColor.GRAY + "Type " + ChatColor.WHITE + ChatColor.ITALIC + "/" + label + " help" + ChatColor.GRAY + " to display commands.");
		}
		else if(args[0].equalsIgnoreCase(COMMAND_HELP))
		{
			if(checkPermission(sender, PERMISSION_HELP))
				return true;

			cmd(sender, label, PERMISSION_HELP, COMMAND_HELP, DESCRIPTION_HELP, ChatColor.YELLOW);
			cmd(sender, label, PERMISSION_RELOAD, COMMAND_RELOAD, DESCRIPTION_RELOAD, ChatColor.YELLOW);
			cmd(sender, label, PERMISSION_BUILD, COMMAND_BUILD, DESCRIPTION_BUILD, ChatColor.YELLOW);
			cmd(sender, label, PERMISSION_LIST, COMMAND_LIST, DESCRIPTION_LIST, ChatColor.YELLOW);
			cmd(sender, label, PERMISSION_ENABLE, COMMAND_ENABLE, DESCRIPTION_ENABLE, ChatColor.YELLOW);
			cmd(sender, label, PERMISSION_DISABLE, COMMAND_DISABLE, DESCRIPTION_DISABLE, ChatColor.YELLOW);
		}
		else if(args[0].equalsIgnoreCase(COMMAND_RELOAD))
		{
			if(checkPermission(sender, PERMISSION_RELOAD))
				return true;

			Limoticon.getInstance().load();
			sender.sendMessage("Limoticon reloaded.");
		}
		else if(args[0].equalsIgnoreCase(COMMAND_BUILD))
		{
			if(checkPermission(sender, PERMISSION_BUILD))
				return true;

			Limoticon limoticon = Limoticon.getInstance();
			ResourcePackBuilder builder = limoticon.getBuilder();

			try
			{
				builder.build();
				sender.sendMessage(ChatColor.GREEN + "Resource pack images successfully built.");
			}
			catch(ResourcePackBuilderException e)
			{
				sender.sendMessage(ChatColor.RED + "An error occurred while building the resource pack images: " + e.getLocalizedMessage());
				e.printStackTrace();
			}
		}
		else if(args[0].equalsIgnoreCase(COMMAND_LIST))
		{
			if(checkPermission(sender, PERMISSION_LIST))
				return true;

			listEmoticonPacks(sender);
		}
		else if(args[0].equalsIgnoreCase(COMMAND_ENABLE))
		{
			if(!(sender instanceof Player))
			{
				sender.sendMessage(ChatColor.RED + "Players only.");
				return true;
			}

			Player player = (Player) sender;

			if(checkPermission(player, PERMISSION_ENABLE))
				return true;

			String packId = joinFromIndex(args, 1);
			EmoticonPack pack = Limoticon.getInstance().getLibrary().getPackMap().get(packId);

			if(pack == null)
			{
				player.sendMessage(ChatColor.RED + "Emoticon pack of id '" + ChatColor.WHITE + packId + ChatColor.RED + "' not found. Try "
				                   + ChatColor.WHITE + ChatColor.ITALIC + "/" + label + " " + COMMAND_LIST + ChatColor.RED + ".");
				return true;
			}

			if(checkPermission(player, pack.getPermissionNode()))
				return true;

			LPlayer lPlayer = LPlayer.get(player);
			List<EmoticonPack> enabledPacks = lPlayer.getEmoticonPacks();

			if(enabledPacks.contains(pack))
			{
				player.sendMessage(ChatColor.RED + "Emoticon pack id '" + ChatColor.WHITE + packId + ChatColor.RED + "' is already enabled. Try "
				                   + ChatColor.WHITE + ChatColor.ITALIC + "/" + label + " " + COMMAND_LIST + ChatColor.RED + ".");
				return true;
			}

			enabledPacks.add(pack);
			listEmoticonPacks(player);
		}
		else if(args[0].equalsIgnoreCase(COMMAND_DISABLE))
		{
			if(!(sender instanceof Player))
			{
				sender.sendMessage(ChatColor.RED + "Players only.");
				return true;
			}

			Player player = (Player) sender;

			if(checkPermission(player, PERMISSION_DISABLE))
				return true;

			String packId = joinFromIndex(args, 1);
			EmoticonPack pack = Limoticon.getInstance().getLibrary().getPackMap().get(packId);

			if(pack == null)
			{
				player.sendMessage(ChatColor.RED + "Emoticon pack of id '" + ChatColor.WHITE + packId + ChatColor.RED + "' not found. Try "
				                   + ChatColor.WHITE + ChatColor.ITALIC + "/" + label + " " + COMMAND_LIST + ChatColor.RED + ".");
				return true;
			}

			LPlayer lPlayer = LPlayer.get(player);
			List<EmoticonPack> enabledPacks = lPlayer.getEmoticonPacks();
			boolean removed = enabledPacks.remove(pack);

			if(!removed)
			{
				player.sendMessage(ChatColor.RED + "Emoticon pack id '" + ChatColor.WHITE + packId + ChatColor.RED + "' is not currently enabled. Try "
				                   + ChatColor.WHITE + ChatColor.ITALIC + "/" + label + " " + COMMAND_LIST + ChatColor.RED + ".");
				return true;
			}

			listEmoticonPacks(player);
		}
		else
		{
			cmd(sender, label, PERMISSION_HELP, COMMAND_HELP, DESCRIPTION_HELP, ChatColor.RED);
			cmd(sender, label, PERMISSION_RELOAD, COMMAND_RELOAD, DESCRIPTION_RELOAD, ChatColor.RED);
			cmd(sender, label, PERMISSION_BUILD, COMMAND_BUILD, DESCRIPTION_BUILD, ChatColor.RED);
			cmd(sender, label, PERMISSION_LIST, COMMAND_LIST, DESCRIPTION_LIST, ChatColor.RED);
			cmd(sender, label, PERMISSION_ENABLE, COMMAND_ENABLE, DESCRIPTION_ENABLE, ChatColor.RED);
			cmd(sender, label, PERMISSION_DISABLE, COMMAND_DISABLE, DESCRIPTION_DISABLE, ChatColor.RED);
		}

		return true;
	}

	private String joinFromIndex(String[] args, int index)
	{
		String[] sub = new String[args.length - index];

		System.arraycopy(args, index, sub, 0, sub.length);

		return Joiner.on(' ').join(sub);
	}

	private void listEmoticonPacks(CommandSender sender)
	{
		for(EmoticonPack pack : getOrderedPacks(sender))
			sender.sendMessage(getPackColor(sender, pack) + pack.getName() + ChatColor.GRAY + " (" + pack.getId() + ") - " + pack.getDescription());
	}

	private List<EmoticonPack> getOrderedPacks(CommandSender sender)
	{
		List<EmoticonPack> packs = Limoticon.getInstance().getLibrary().getPacks();

		if(!(sender instanceof Player))
			return packs;

		Player player = (Player) sender;
		LPlayer lPlayer = LPlayer.get(player);
		List<EmoticonPack> enabledPacks = lPlayer.getEmoticonPacks();
		List<EmoticonPack> result = Lists.newArrayList(enabledPacks);

		for(EmoticonPack pack : packs)
			if(!result.contains(pack))
				result.add(pack);

		return result;
	}

	private ChatColor getPackColor(CommandSender sender, EmoticonPack pack)
	{
		if(!(sender instanceof Player))
			return ChatColor.WHITE;

		Player player = (Player) sender;
		LPlayer lPlayer = LPlayer.get(player);
		List<EmoticonPack> enabledPacks = lPlayer.getEmoticonPacks();

		if(enabledPacks.contains(pack))
			return ChatColor.GREEN;

		String permissionNode = pack.getPermissionNode();

		return player.hasPermission(permissionNode) ? ChatColor.GRAY : ChatColor.RED;
	}

	private boolean checkPermission(CommandSender sender, String permission)
	{
		if(!sender.hasPermission(permission))
		{
			sender.sendMessage(ChatColor.RED + "You are not allowed to run this command. (" + permission + ")");
			return true;
		}

		return false;
	}

	private static void cmd(CommandSender sender, String label, String command, String description, ChatColor color)
	{
		sender.sendMessage(color + "/" + label + " " + command + ChatColor.GRAY + " - " + description);
	}

	private static void cmd(CommandSender sender, String label, String permission, String command, String description, ChatColor color)
	{
		if(sender.hasPermission(permission))
			cmd(sender, label, command, description, color);
	}
}
