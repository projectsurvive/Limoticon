package cz.projectsurvive.limeth.limoticon;

/**
 * @author Limeth
 */
public class ResourcePackBuilderException extends Exception
{
	public ResourcePackBuilderException()
	{
	}

	public ResourcePackBuilderException(String message)
	{
		super(message);
	}

	public ResourcePackBuilderException(String message, Throwable cause)
	{
		super(message, cause);
	}

	public ResourcePackBuilderException(Throwable cause)
	{
		super(cause);
	}

	public ResourcePackBuilderException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
	}
}
