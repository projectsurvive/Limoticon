package cz.projectsurvive.limeth.limoticon;

import cz.projectsurvive.limeth.limoticon.dbi.Storage;
import cz.projectsurvive.limeth.limoticon.listeners.ConnectionListener;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.File;

/**
 * @author Limeth
 */
public class Limoticon extends JavaPlugin
{
	private static final String FILE_CONFIGURATION_NAME = "config.yml";
	private static Limoticon instance;
	@Loadable
	private EmoticonLibrary library;
	@Loadable
	private ResourcePackBuilder builder;
	@Loadable
	private Storage storage;

	@Override
	public void onEnable()
	{
		instance = this;

		if(!load())
			return;

		if(!library.indexEmoticons())
			return;

		storage.setup();
		registerChat();
		registerCommands();
		registerListeners();
	}

	@Override
	public void onDisable()
	{
		instance = null;
	}

	public boolean load()
	{
		try
		{
			Loadable.Executor.load(this, getConfigurationFile());
			return true;
		}
		catch(Loadable.LoadableException e)
		{
			e.printStackTrace();
			return false;
		}
	}

	private void registerChat()
	{
		new EmoticonChatHandler().register();
	}

	private void registerCommands()
	{
		Bukkit.getPluginCommand("limoticon").setExecutor(new LimoticonCommandExecutor());
	}

	private void registerListeners()
	{
		PluginManager pluginManager = Bukkit.getPluginManager();

		pluginManager.registerEvents(new ConnectionListener(), this);
	}

	public EmoticonLibrary getLibrary()
	{
		return library;
	}

	public ResourcePackBuilder getBuilder()
	{
		return builder;
	}

	public Storage getStorage()
	{
		return storage;
	}

	public static Limoticon getInstance()
	{
		return instance;
	}

	public static File getConfigurationFile()
	{
		return new File(getInstance().getDataFolder(), FILE_CONFIGURATION_NAME);
	}
}
