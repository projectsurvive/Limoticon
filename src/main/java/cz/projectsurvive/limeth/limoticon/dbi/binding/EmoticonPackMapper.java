package cz.projectsurvive.limeth.limoticon.dbi.binding;

import cz.projectsurvive.limeth.limoticon.EmoticonPack;
import cz.projectsurvive.limeth.limoticon.Limoticon;
import cz.projectsurvive.limeth.limoticon.dbi.LPlayerDAO;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * @author Limeth
 */
public class EmoticonPackMapper implements ResultSetMapper<EmoticonPack>
{
	@Override
	public EmoticonPack map(int i, ResultSet resultSet, StatementContext statementContext) throws SQLException
	{
		return Limoticon.getInstance().getLibrary().getPackMap().get(resultSet.getString(LPlayerDAO.FIELD_EMOTICONPACK));
	}
}
