package cz.projectsurvive.limeth.limoticon.dbi.binding;

import cz.projectsurvive.limeth.limoticon.EmoticonPack;
import org.skife.jdbi.v2.SQLStatement;
import org.skife.jdbi.v2.sqlobject.Binder;
import org.skife.jdbi.v2.sqlobject.BinderFactory;

import java.lang.annotation.Annotation;

/**
 * @author Limeth
 */
public class BindEmoticonPackFactory implements BinderFactory
{
	@Override
	public Binder build(Annotation annotation)
	{
		return new Binder<BindEmoticonPack, EmoticonPack>()
		{
			@Override
			public void bind(SQLStatement q, BindEmoticonPack bind, EmoticonPack pack)
			{
				final String prefix;
				if (BindEmoticonPack.BARE_BINDING.equals(bind.value())) {
					prefix = "";
				}
				else {
					prefix = bind.value() + ".";
				}

				q.bind(prefix + "id", pack != null ? pack.getId() : null);
				q.bind(prefix + "name", pack != null ? pack.getName() : null);
				q.bind(prefix + "description", pack != null ? pack.getDescription() : null);
			}
		};
	}
}
