package cz.projectsurvive.limeth.limoticon.dbi;

import cz.projectsurvive.limeth.limoticon.EmoticonPack;
import cz.projectsurvive.limeth.limoticon.dbi.binding.BindEmoticonPack;
import cz.projectsurvive.limeth.limoticon.dbi.binding.EmoticonPackMapper;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.sqlobject.*;
import org.skife.jdbi.v2.sqlobject.customizers.Define;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;
import org.skife.jdbi.v2.sqlobject.stringtemplate.UseStringTemplate3StatementLocator;

import java.util.List;

import static cz.projectsurvive.limeth.limoticon.dbi.SQLHelper.column;
import static cz.projectsurvive.limeth.limoticon.dbi.SQLHelper.prepareTable;

/**
 * @author Limeth
 */
@RegisterMapper({EmoticonPackMapper.class})
@UseStringTemplate3StatementLocator
public interface LPlayerDAO
{
	String FIELD_PLAYER            = "player";
	String FIELD_EMOTICONPACK      = "emoticon_pack";
	String FIELD_ORDER             = "order";
	String PROPERTIES_PLAYER       = "varchar(16) COLLATE utf8_general_ci NOT NULL";
	String PROPERTIES_EMOTICONPACK = "varchar(32) COLLATE utf8_general_ci NOT NULL";
	String PROPERTIES_ORDER        = "int NOT NULL";

	static void prepareTablePlayers(DBI dbi, String tablePlayers)
	{
		prepareTable(dbi, tablePlayers, FIELD_PLAYER, column(FIELD_PLAYER, PROPERTIES_PLAYER));
	}

	static void prepareTableEmoticonPacks(DBI dbi, String tableEmoticonPacks)
	{
		prepareTable(dbi, tableEmoticonPacks, new String[]{FIELD_PLAYER, FIELD_EMOTICONPACK}, new String[]{FIELD_PLAYER, FIELD_ORDER}, column(FIELD_PLAYER, PROPERTIES_PLAYER), column(FIELD_EMOTICONPACK, PROPERTIES_EMOTICONPACK), column(FIELD_ORDER, PROPERTIES_ORDER));
	}

	static boolean isPlayerPresent(LPlayerDAO dao, String tablePlayers, String username)
	{
		return dao.selectPlayer(tablePlayers, username) != null;
	}

	static boolean deletePlayer(LPlayerDAO dao, String tablePlayers, String username)
	{
		return dao.deletePlayer(tablePlayers, username) > 0;
	}

	@SqlQuery("SELECT `" + FIELD_PLAYER + "` FROM <table> WHERE `" + FIELD_PLAYER + "` = :" + FIELD_PLAYER)
	String selectPlayer(@Define("table") String tableName, @Bind(FIELD_PLAYER) String username);

	@SqlUpdate("INSERT IGNORE INTO <table> (`" + FIELD_PLAYER + "`) VALUES (:" + FIELD_PLAYER + ")")
	void insertPlayer(@Define("table") String tableName, @Bind(FIELD_PLAYER) String username);

	@SqlUpdate("DELETE FROM <table> WHERE `" + FIELD_PLAYER + "` = :" + FIELD_PLAYER)
	int deletePlayer(@Define("table") String tableName, @Bind(FIELD_PLAYER) String username);

	@SqlQuery("SELECT * FROM <table> WHERE `" + FIELD_PLAYER + "` = :" + FIELD_PLAYER + " ORDER BY `" + FIELD_ORDER + "`")
	List<EmoticonPack> selectEmoticonPacks(@Define("table") String tableName, @Bind(FIELD_PLAYER) String username);

	@SqlBatch("INSERT INTO <table> (`" + FIELD_PLAYER + "`, `" + FIELD_EMOTICONPACK + "`, `" + FIELD_ORDER + "`) VALUES (:" + FIELD_PLAYER + ", :" + FIELD_EMOTICONPACK + ".id, :" + FIELD_ORDER + ")")
	void insertEmoticonPacks(@Define("table") String tableName, @Bind(FIELD_PLAYER) String username, @BindEmoticonPack(FIELD_EMOTICONPACK) Iterable<EmoticonPack> emoticonPacks, @Bind(FIELD_ORDER) Iterable<Integer> order);

	@SqlUpdate("DELETE FROM <table> WHERE `" + FIELD_PLAYER + "` = :" + FIELD_PLAYER)
	void clearEmoticonPacks(@Define("table") String tableName, @Bind(FIELD_PLAYER) String username);

	void close();
}
