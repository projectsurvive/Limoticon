package cz.projectsurvive.limeth.limoticon.dbi;

import org.skife.jdbi.v2.Batch;
import org.skife.jdbi.v2.DBI;
import org.skife.jdbi.v2.Handle;

public class SQLHelper
{
    private SQLHelper() {}

    static void prepareTable(DBI dbi, String table, String[] primaryKeys, String[] uniqueKeys, Column... columns)
    {
        Handle handle = dbi.open();
        Batch batch = handle.createBatch();
        StringBuilder csvColumns = new StringBuilder();
	    StringBuilder primaryKeysString = primaryKeys.length <= 0 ? null : new StringBuilder('`' + primaryKeys[0] + '`');
	    StringBuilder uniqueKeysString = uniqueKeys.length <= 0 ? null : new StringBuilder('`' + uniqueKeys[0] + '`');

        for(int i = 0; i < columns.length; i++)
        {
            if(i > 0)
                csvColumns.append(", ");

            csvColumns.append(columns[i].toString());
        }

	    if(primaryKeysString != null)
		    for(int i = 1; i < primaryKeys.length; i++)
			    primaryKeysString.append(", ").append('`').append(primaryKeys[i]).append('`');

	    if(uniqueKeysString != null)
		    for(int i = 1; i < uniqueKeys.length; i++)
			    uniqueKeysString.append(", ").append('`').append(uniqueKeys[i]).append('`');

        batch.add("CREATE TABLE IF NOT EXISTS " + table + " (" +
                csvColumns +
                (primaryKeysString != null ? (", PRIMARY KEY (" + primaryKeysString + ")") : "") +
                (uniqueKeysString != null ? (", UNIQUE KEY (" + uniqueKeysString + ")") : "") +
                ") ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci");

        for(Column column : columns)
            prepareColumn(batch, table, column.getName(), column.getProperties());

        batch.execute();
        handle.close();
    }

	static void prepareTable(DBI dbi, String table, String[] primaryKeys, Column... columns)
	{
		prepareTable(dbi, table, primaryKeys, new String[0], columns);
	}

	static void prepareTable(DBI dbi, String table, String primaryKeys, Column... columns)
	{
		prepareTable(dbi, table, primaryKeys != null ? new String[] {primaryKeys} : null, columns);
	}

    public static void prepareColumn(Batch batch, String tableName, String columnName, String type)
    {
        batch.add("SET @s = (SELECT IF(\n" +
                "    (SELECT COUNT(*)\n" +
                "        FROM INFORMATION_SCHEMA.COLUMNS\n" +
                "        WHERE table_name = '" + tableName + "'\n" +
                "        AND table_schema = DATABASE()\n" +
                "        AND column_name = '" + columnName + "'\n" +
                "    ) > 0,\n" +
                "    \"SELECT 1\",\n" +
                "    \"ALTER TABLE `" + tableName + "` ADD `" + columnName + "` " + type + "\"\n" +
                "))");
        batch.add("PREPARE stmt FROM @s");
        batch.add("EXECUTE stmt");
        batch.add("DEALLOCATE PREPARE stmt");
    }

    public static Column column(String name, String properties)
    {
        return new Column(name, properties);
    }

    public static class Column
    {
        private final String name;
        private final String properties;

        public Column(String name, String properties)
        {
            this.name = name;
            this.properties = properties;
        }

        public String getName() {
            return name;
        }

        public String getProperties() {
            return properties;
        }

        @Override
        public String toString() {
            return "`" + name + "` " + properties;
        }
    }
}
