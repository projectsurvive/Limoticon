package cz.projectsurvive.limeth.limoticon.dbi;

import com.google.common.collect.Lists;
import cz.projectsurvive.limeth.limoticon.*;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.skife.jdbi.v2.DBI;

import java.util.Collection;
import java.util.List;

/**
 * @author Limeth
 */
public class Storage
{
	private static final String SECTION                     = "storage";
	private static final String KEY_PREFIX                  = SECTION + '.';
	private static final String KEY_HOST                    = KEY_PREFIX + "host";
	private static final String KEY_PORT                    = KEY_PREFIX + "port";
	private static final String KEY_USERNAME                = KEY_PREFIX + "username";
	private static final String KEY_PASSWORD                = KEY_PREFIX + "password";
	private static final String KEY_DATABASE                = KEY_PREFIX + "database";
	private static final String KEY_TABLE_PLAYERS           = KEY_PREFIX + "tables.players";
	private static final String KEY_TABLE_EMOTICONPACKS     = KEY_PREFIX + "tables.emoticonPacks";
	private static final String DEFAULT_HOST                = "localhost";
	private static final int    DEFAULT_PORT                = 3306;
	private static final String DEFAULT_USERNAME            = "root";
	private static final String DEFAULT_PASSWORD            = "nbusr123";
	private static final String DEFAULT_DATABASE            = "limoticon";
	private static final String DEFAULT_TABLE_PLAYERS       = "limoticon_players";
	private static final String DEFAULT_TABLE_EMOTICONPACKS = "limoticon_packs";
	private String host;
	private int port;
	private String username;
	private String password;
	private String database;
	private String tablePlayers;
	private String tableEmoticonPacks;
	private DBI dbi;

	private Storage() {}

	public static void prepare(YamlConfiguration yml) throws Loadable.LoadableException
	{
		prepare(yml, KEY_HOST, DEFAULT_HOST);
		prepare(yml, KEY_PORT, DEFAULT_PORT);
		prepare(yml, KEY_USERNAME, DEFAULT_USERNAME);
		prepare(yml, KEY_PASSWORD, DEFAULT_PASSWORD);
		prepare(yml, KEY_DATABASE, DEFAULT_DATABASE);
		prepare(yml, KEY_TABLE_PLAYERS, DEFAULT_TABLE_PLAYERS);
		prepare(yml, KEY_TABLE_EMOTICONPACKS, DEFAULT_TABLE_EMOTICONPACKS);
	}

	private static void prepare(YamlConfiguration yml, String key, Object defaultValue)
	{
		if(!yml.contains(key))
			yml.set(key, defaultValue);
	}

	public static Storage load(YamlConfiguration yml)
	{
		Storage storage = new Storage();
		storage.host = yml.getString(KEY_HOST, DEFAULT_HOST);
		storage.port = yml.getInt(KEY_PORT, DEFAULT_PORT);
		storage.username = yml.getString(KEY_USERNAME, DEFAULT_USERNAME);
		storage.password = yml.getString(KEY_PASSWORD, DEFAULT_PASSWORD);
		storage.database = yml.getString(KEY_DATABASE, DEFAULT_DATABASE);
		storage.tablePlayers = yml.getString(KEY_TABLE_PLAYERS, DEFAULT_TABLE_PLAYERS);
		storage.tableEmoticonPacks = yml.getString(KEY_TABLE_EMOTICONPACKS, DEFAULT_TABLE_EMOTICONPACKS);

		return storage;
	}

	public void setup()
	{
		dbi = new DBI(getURL(), username, password);

		setupDatabase();
	}

	private LPlayerDAO openLPlayerDAO()
	{
		return dbi.open(LPlayerDAO.class);
	}

	private void setupDatabase()
	{
		LPlayerDAO.prepareTablePlayers(dbi, tablePlayers);
		LPlayerDAO.prepareTableEmoticonPacks(dbi, tableEmoticonPacks);
	}

	public LPlayer loadPlayer(Player player)
	{
		String username = player.getName();
		List<EmoticonPack> packs;
		LPlayerDAO playerDAO = openLPlayerDAO();

		if(!LPlayerDAO.isPlayerPresent(playerDAO, tablePlayers, username))
		{
			Limoticon limoticon = Limoticon.getInstance();
			EmoticonLibrary library = limoticon.getLibrary();

			packs = library.getDefaultPacks();
		}
		else
			packs = playerDAO.selectEmoticonPacks(tableEmoticonPacks, username);

		playerDAO.close();

		return new LPlayer(player, packs);
	}

	public void savePlayer(LPlayer lPlayer)
	{
		Player player = lPlayer.getPlayer();
		String username = player.getName();
		List<EmoticonPack> emoticonPacks = Lists.newArrayList(lPlayer.getEmoticonPacks());
		LPlayerDAO playerDAO = openLPlayerDAO();

		playerDAO.insertPlayer(tablePlayers, username);
		playerDAO.clearEmoticonPacks(tableEmoticonPacks, username);
		playerDAO.insertEmoticonPacks(tableEmoticonPacks, username, emoticonPacks, orderOf(emoticonPacks));

		playerDAO.close();
	}

	private static List<Integer> orderOf(Collection<?> collection)
	{
		List<Integer> order = Lists.newArrayList();

		for(int i = 0; i < collection.size(); i++)
			order.add(i);

		return order;
	}

	public String getURL()
	{
		return "jdbc:mysql://" + getHost() + ":" + getPort() + "/" + getDatabase();
	}

	public String getHost()
	{
		return host;
	}

	public int getPort()
	{
		return port;
	}

	public String getDatabase()
	{
		return database;
	}

	public String getUsername()
	{
		return username;
	}

	public String getPassword()
	{
		return password;
	}

	public String getTablePlayers()
	{
		return tablePlayers;
	}

	public String getTableEmoticonPacks()
	{
		return tableEmoticonPacks;
	}
}
