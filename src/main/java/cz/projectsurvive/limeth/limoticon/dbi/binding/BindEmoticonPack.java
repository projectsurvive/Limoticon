package cz.projectsurvive.limeth.limoticon.dbi.binding;

import org.skife.jdbi.v2.sqlobject.BindingAnnotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author Limeth
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.PARAMETER})
@BindingAnnotation(BindEmoticonPackFactory.class)
public @interface BindEmoticonPack
{
	final String BARE_BINDING = "___jdbi_bare___";
	String value() default BARE_BINDING;
}
