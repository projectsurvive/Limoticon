package cz.projectsurvive.limeth.limoticon;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import org.bukkit.entity.Player;

import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * @author Limeth
 */
public class LPlayer
{
	private static Map<Player, LPlayer> playerMap = Maps.newHashMap();

	public static synchronized Collection<LPlayer> list()
	{
		return playerMap.values();
	}

	public static synchronized LPlayer unregister(Player player)
	{
		return playerMap.remove(player);
	}

	public static synchronized LPlayer get(Player player)
	{
		return playerMap.get(player);
	}

	public static synchronized LPlayer loadAndRegister(Player player)
	{
		LPlayer lPlayer = Limoticon.getInstance().getStorage().loadPlayer(player);

		register(lPlayer);

		return lPlayer;
	}

	public static synchronized LPlayer saveAndUnregister(Player player)
	{
		LPlayer result = unregister(player);

		result.save();

		return result;
	}

	private static synchronized void register(LPlayer msPlayer)
	{
		msPlayer.setJoinedAt(Instant.now());

		Player player = msPlayer.getPlayer();

		playerMap.put(player, msPlayer);
	}

	public static synchronized void clearOnlinePlayers()
	{
		playerMap.clear();
	}

	private final Player             player;
	private final List<EmoticonPack> emoticonPacks;
	private       Instant            joinedAt;

	public LPlayer(Player player, List<EmoticonPack> emoticonPacks)
	{
		Preconditions.checkNotNull(player, "The player must not be null!");

		this.player = player;
		this.emoticonPacks = Lists.newArrayList(emoticonPacks);
	}

	public void save()
	{
		Limoticon.getInstance().getStorage().savePlayer(this);
	}

	public Player getPlayer()
	{
		return player;
	}

	public List<EmoticonPack> getEmoticonPacks()
	{
		return emoticonPacks;
	}

	public Instant getJoinedAt()
	{
		return joinedAt;
	}

	private void setJoinedAt(Instant joinedAt)
	{
		this.joinedAt = joinedAt;
	}
}
