package cz.projectsurvive.limeth.limoticon;

import com.google.common.collect.Maps;
import org.bukkit.configuration.file.YamlConfiguration;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.Optional;
import java.util.Vector;

/**
 * @author Limeth
 */
public class ResourcePackBuilder
{
	private static final String KEY_BUILDER           = "builder";
	private static final String KEY_OFFSET_SOURCE_X   = "builder.offset.source.x";
	private static final String KEY_OFFSET_SOURCE_Y   = "builder.offset.source.y";
	private static final String KEY_OFFSET_TARGET_X   = "builder.offset.target.x";
	private static final String KEY_OFFSET_TARGET_Y   = "builder.offset.target.y";
	private static final String DIRECTORY_SOURCE_NAME = "source";
	private static final String DIRECTORY_TARGET_NAME = "target";
	private static final int    CHARACTERS_PER_ROW    = 16;
	private static final int    CHARACTERS_PER_COLUMN = 16;
	private static final int    CHARACTERS_PER_PAGE   = CHARACTERS_PER_ROW * CHARACTERS_PER_COLUMN;
	private static final int    CHARACTER_WIDTH       = 16;
	private static final int    CHARACTER_HEIGHT      = 16;
	private int offsetSourceX, offsetSourceY, offsetTargetX, offsetTargetY;
	private Map<Integer, BufferedImage> imageMap = Maps.newHashMap();

	public ResourcePackBuilder(int offsetSourceX, int offsetSourceY, int offsetTargetX, int offsetTargetY)
	{
		this.offsetSourceX = offsetSourceX;
		this.offsetSourceY = offsetSourceY;
		this.offsetTargetX = offsetTargetX;
		this.offsetTargetY = offsetTargetY;
	}

	public static void prepare(YamlConfiguration yml)
	{
		if(!yml.contains(KEY_BUILDER))
		{
			yml.set(KEY_OFFSET_SOURCE_X, 0);
			yml.set(KEY_OFFSET_SOURCE_Y, 0);
			yml.set(KEY_OFFSET_TARGET_X, 0);
			yml.set(KEY_OFFSET_TARGET_Y, 0);
		}

		if(!getSourceDirectory().exists())
			getSourceDirectory().mkdirs();

		if(!getTargetDirectory().exists())
			getTargetDirectory().mkdirs();
	}

	public static ResourcePackBuilder load(YamlConfiguration yml)
	{
		int offsetSourceX = yml.getInt(KEY_OFFSET_SOURCE_X);
		int offsetSourceY = yml.getInt(KEY_OFFSET_SOURCE_Y);
		int offsetTargetX = yml.getInt(KEY_OFFSET_TARGET_X);
		int offsetTargetY = yml.getInt(KEY_OFFSET_TARGET_Y);

		return new ResourcePackBuilder(offsetSourceX, offsetSourceY, offsetTargetX, offsetTargetY);
	}

	public void build() throws ResourcePackBuilderException
	{
		Limoticon limoticon = Limoticon.getInstance();
		EmoticonLibrary library = limoticon.getLibrary();

		clearImages();

		for(EmoticonPack pack : library.getPacks())
			for(Emoticon emoticon : library.getEmoticons())
			{
				Optional<Integer> emoticonCharacterCode = library.getCharacterCode(pack, emoticon);

				if(!emoticonCharacterCode.isPresent())
					continue;

				render(pack, emoticon, emoticonCharacterCode.get());
			}

		saveImages();
		clearImages();
	}

	private void render(EmoticonPack pack, Emoticon emoticon, int characterCode) throws ResourcePackBuilderException
	{
		BufferedImage targetImage = getPageImage(characterCode);
		BufferedImage sourceImage = loadEmoticonImage(pack, emoticon);
		BufferedImage subSourceImage = sourceImage.getSubimage(offsetSourceX, offsetSourceY, CHARACTER_WIDTH, CHARACTER_HEIGHT);
		Vector<Integer> targetLocation = getLocationWithinPage(characterCode);
		Graphics2D graphics = targetImage.createGraphics();

		graphics.setComposite(AlphaComposite.getInstance(AlphaComposite.CLEAR));
		graphics.fillRect(targetLocation.get(0), targetLocation.get(1), CHARACTER_WIDTH, CHARACTER_HEIGHT);

		graphics.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER));
		graphics.drawImage(subSourceImage, targetLocation.get(0), targetLocation.get(1), null);
	}

	private Vector<Integer> getLocationWithinPage(int characterCode)
	{
		int index = characterCode % CHARACTERS_PER_PAGE;
		int x = (index % CHARACTERS_PER_ROW) * CHARACTER_WIDTH + offsetTargetX;
		int y = (index / CHARACTERS_PER_ROW) * CHARACTER_HEIGHT + offsetTargetY;
		Vector<Integer> vector = new Vector<>(2);

		vector.add(0, x);
		vector.add(1, y);

		return vector;
	}

	private void saveImages() throws ResourcePackBuilderException
	{
		for(Map.Entry<Integer, BufferedImage> entry : imageMap.entrySet())
		{
			int minCharCode = entry.getKey();
			File sourceFile = getSourceFile(minCharCode);
			File targetFile = new File(getTargetDirectory(), sourceFile.getName());
			BufferedImage image = entry.getValue();

			try
			{
				ImageIO.write(image, "PNG", targetFile);
			}
			catch(IOException e)
			{
				throw new ResourcePackBuilderException("Could not save image: " + targetFile.getAbsolutePath(), e);
			}
		}
	}

	public void clearImages()
	{
		imageMap.clear();
	}

	public BufferedImage getSourceCharacterImage(int characterCode) throws ResourcePackBuilderException
	{
		BufferedImage sourcePageImage = getPageImage(characterCode);
		Vector<Integer> targetLocation = getLocationWithinPage(characterCode);

		return sourcePageImage.getSubimage(targetLocation.get(0), targetLocation.get(1), CHARACTER_WIDTH, CHARACTER_HEIGHT);
	}

	public boolean isCharacterDefined(int characterCode) throws ResourcePackBuilderException
	{
		BufferedImage image = getSourceCharacterImage(characterCode);
		int size = image.getWidth() * image.getHeight();
		int[] alphaRaster = image.getAlphaRaster().getPixels(0, 0, CHARACTER_WIDTH, CHARACTER_HEIGHT, new int[size]);

		for(int alpha : alphaRaster)
			if(alpha != 0)
				return true;

		return false;
	}

	private BufferedImage loadEmoticonImage(EmoticonPack pack, Emoticon emoticon) throws ResourcePackBuilderException
	{
		File emoticonFile = pack.getEmoticonFile(emoticon);

		try
		{
			BufferedImage img = ImageIO.read(emoticonFile);

			if(img == null)
				throw new ResourcePackBuilderException("Source image not found: " + emoticonFile.getAbsolutePath());

			return img;
		}
		catch(IOException e)
		{
			throw new ResourcePackBuilderException("Could not load emoticon file: " + emoticonFile.getAbsolutePath(), e);
		}
	}

	private BufferedImage getPageImage(int characterCode) throws ResourcePackBuilderException
	{
		int minCharCode = getLowestCharacterOfPage(characterCode);
		BufferedImage image = imageMap.get(minCharCode);

		if(image != null)
			return image;

		File file = getSourceFile(characterCode);

		try
		{
			image = ImageIO.read(file);
		}
		catch(IOException e)
		{
			throw new ResourcePackBuilderException("Could not read image: " + file.getAbsolutePath(), e);
		}

		BufferedImage resultImage = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_ARGB);
		Graphics2D g2d = resultImage.createGraphics();
		g2d.drawImage(image, 0, 0, null);
		g2d.dispose();

		imageMap.put(minCharCode, resultImage);

		return resultImage;
	}

	private static int getLowestCharacterOfPage(int characterCode)
	{
		return (characterCode / CHARACTERS_PER_PAGE) * CHARACTERS_PER_PAGE;
	}

	public static String getImageFileName(int characterCode) throws ResourcePackBuilderException
	{
		int index = characterCode / CHARACTERS_PER_PAGE;
		String hexIndex = Integer.toHexString(index);

		if(index < 0 || hexIndex.length() > 2)
			throw new ResourcePackBuilderException("Character code out of bounds: " + characterCode);

		while(hexIndex.length() < 2)
			hexIndex = "0" + hexIndex;

		return "unicode_page_" + hexIndex + ".png";
	}

	public static File getSourceFile(int characterCode) throws ResourcePackBuilderException
	{
		return new File(getSourceDirectory(), getImageFileName(characterCode));
	}

	public static File getTargetFile(int characterCode) throws ResourcePackBuilderException
	{
		return new File(getTargetDirectory(), getImageFileName(characterCode));
	}

	public static File getSourceDirectory()
	{
		return new File(Limoticon.getInstance().getDataFolder(), DIRECTORY_SOURCE_NAME);
	}

	public static File getTargetDirectory()
	{
		return new File(Limoticon.getInstance().getDataFolder(), DIRECTORY_TARGET_NAME);
	}
}
