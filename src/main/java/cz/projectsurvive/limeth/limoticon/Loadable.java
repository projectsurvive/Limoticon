package cz.projectsurvive.limeth.limoticon;

import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Limeth
 * Marks a class that is loaded by the Limoticon plugin in the onEnable method.
 *
 * Must declare the following methods:
 * {@code public static void prepare(YamlConfiguration yml)}
 * {@code public static Object load(YamlConfiguration yml)}
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Loadable
{
	public static final class Executor
	{
		public static YamlConfiguration load(Object holder, File file) throws LoadableException
		{
			Class<?> holderClass = holder.getClass();
			List<Field> appropriateFields = Arrays.stream(holderClass.getDeclaredFields())
			                                      .filter(field -> field.isAnnotationPresent(Loadable.class))
			                                      .collect(Collectors.toList());
			List<Class<?>> fieldTypes = appropriateFields.stream()
			                                             .map(Field::getType)
			                                             .collect(Collectors.toList());
			YamlConfiguration yml = prepare(file, fieldTypes);

			for(Field field : appropriateFields)
			{
				Class<?> type = field.getType();
				Object value = load(type, yml);

				if(!field.isAccessible())
					field.setAccessible(true);

				try
				{
					field.set(holder, value);
				}
				catch(IllegalAccessException e)
				{
					throw new LoadableException("Could not set value of field " + field.getName() + ".", e);
				}
			}

			return yml;
		}

		private static YamlConfiguration prepare(File file, List<Class<?>> types) throws LoadableException
		{
			YamlConfiguration yml = YamlConfiguration.loadConfiguration(file);

			for(Class<?> type : types)
				prepare(type, yml);

			try
			{
				yml.save(file);
			}
			catch(IOException e)
			{
				System.out.println("Could not save prepared " + file.getName() + ".");
				e.printStackTrace();
			}

			return yml;
		}

		private static void prepare(Class<?> loadable, YamlConfiguration yml) throws LoadableException
		{
			try
			{
				loadable.getDeclaredMethod("prepare", YamlConfiguration.class).invoke(null, yml);
			}
			catch(IllegalAccessException e)
			{
				throw new LoadableException("The prepare(YamlConfiguration) method of class " + loadable.getCanonicalName() + " was not accessible.", e);
			}
			catch(InvocationTargetException e)
			{
				throw new LoadableException("The prepare(YamlConfiguration) method of class " + loadable.getCanonicalName() + " failed to execute successfully.", e);
			}
			catch(NoSuchMethodException e)
			{
				throw new LoadableException("The prepare(YamlConfiguration) method of class " + loadable.getCanonicalName() + " is missing.", e);
			}
		}

		private static <T> T load(Class<T> loadable, YamlConfiguration yml) throws LoadableException
		{
			try
			{
				return (T) loadable.getDeclaredMethod("load", YamlConfiguration.class).invoke(null, yml);
			}
			catch(ClassCastException e)
			{
				throw new LoadableException("The load(YamlConfiguration) method of class " + loadable.getCanonicalName() + " must return a value of it's type.", e);
			}
			catch(IllegalAccessException e)
			{
				throw new LoadableException("The load(YamlConfiguration) method of class " + loadable.getCanonicalName() + " was not accessible.", e);
			}
			catch(InvocationTargetException e)
			{
				throw new LoadableException("The load(YamlConfiguration) method of class " + loadable.getCanonicalName() + " failed to execute successfully.", e);
			}
			catch(NoSuchMethodException e)
			{
				throw new LoadableException("The load(YamlConfiguration) method of class " + loadable.getCanonicalName() + " is missing.", e);
			}
		}
	}

	public static class LoadableException extends Exception
	{
		public LoadableException()
		{
		}

		public LoadableException(String message)
		{
			super(message);
		}

		public LoadableException(String message, Throwable cause)
		{
			super(message, cause);
		}

		public LoadableException(Throwable cause)
		{
			super(cause);
		}

		public LoadableException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
		{
			super(message, cause, enableSuppression, writableStackTrace);
		}
	}
}
