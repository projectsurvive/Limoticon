package cz.projectsurvive.limeth.limoticon;

import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import org.bukkit.configuration.ConfigurationSection;

import java.io.File;
import java.util.Map;
import java.util.Set;

/**
 * @author Limeth
 */
public class EmoticonPack
{
	public static final  String       KEY_NAME               = "name";
	public static final  String       KEY_DESCRIPTION        = "description";
	public static final  String       KEY_DEFAULT            = "default";
	private static final String       PERMISSION_NODE_PREFIX = "limoticon.pack.";
	private final String  id;
	private       String  name;
	private       String  description;
	private       boolean def;
	private Map<String, File> emoticonIdsToFiles = Maps.newHashMap();

	public EmoticonPack(String id, String name, String description, boolean def)
	{
		Preconditions.checkNotNull(id);
		Preconditions.checkNotNull(name);
		Preconditions.checkNotNull(description);

		this.id = id;
		this.name = name;
		this.description = description;
		this.def = def;
	}

	public static EmoticonPack load(ConfigurationSection section)
	{
		String id = section.getName();
		String name = section.getString(KEY_NAME);
		String description = section.getString(KEY_DESCRIPTION);
		boolean def = section.getBoolean(KEY_DEFAULT);
		EmoticonPack pack = new EmoticonPack(id, name, description, def);

		pack.loadEmoticonFiles();

		return pack;
	}

	public static File getDirectory(String packId)
	{
		return new File(EmoticonLibrary.getDirectory(), packId);
	}

	private void loadEmoticonFiles()
	{
		emoticonIdsToFiles.clear();

		File[] emoticonFiles = getDirectory().listFiles();

		if(emoticonFiles != null)
			for(File emoticonFile : emoticonFiles)
				if(emoticonFile.isFile() && !emoticonFile.getName().equals("Thumbs.db"))
				{
					String emoticonId = emoticonFile.getName();

					if(emoticonId.indexOf(".") > 0)
						emoticonId = emoticonId.substring(0, emoticonId.lastIndexOf("."));

					emoticonIdsToFiles.put(emoticonId, emoticonFile);
				}
	}

	public String getId()
	{
		return id;
	}

	public String getName()
	{
		return name;
	}

	public String getDescription()
	{
		return description;
	}

	public boolean isDefault()
	{
		return def;
	}

	public Set<String> getEmoticonIds()
	{
		return emoticonIdsToFiles.keySet();
	}

	public Map<String, File> getEmoticonIdsToFiles()
	{
		return Maps.newHashMap(emoticonIdsToFiles);
	}

	public File getDirectory()
	{
		return getDirectory(id);
	}

	public File getEmoticonFile(Emoticon emoticon)
	{
		return emoticonIdsToFiles.get(emoticon.getId());
	}

	public String getPermissionNode()
	{
		return PERMISSION_NODE_PREFIX + id;
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;
		if(o == null || getClass() != o.getClass())
			return false;

		EmoticonPack that = (EmoticonPack) o;

		if(def != that.def)
			return false;
		if(description != null ? !description.equals(that.description) : that.description != null)
			return false;
		if(!emoticonIdsToFiles.equals(that.emoticonIdsToFiles))
			return false;
		if(!id.equals(that.id))
			return false;
		if(name != null ? !name.equals(that.name) : that.name != null)
			return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		int result = id.hashCode();
		result = 31 * result + (name != null ? name.hashCode() : 0);
		result = 31 * result + (description != null ? description.hashCode() : 0);
		result = 31 * result + (def ? 1 : 0);
		result = 31 * result + emoticonIdsToFiles.hashCode();
		return result;
	}

	@Override
	public String toString()
	{
		return "EmoticonPack{" +
		       "id='" + id + '\'' +
		       ", name='" + name + '\'' +
		       ", description='" + description + '\'' +
		       ", def=" + def +
		       ", emoticonIdsToFiles=" + emoticonIdsToFiles +
		       '}';
	}
}
