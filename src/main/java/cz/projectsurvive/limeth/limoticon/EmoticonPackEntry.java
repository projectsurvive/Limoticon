package cz.projectsurvive.limeth.limoticon;

import com.google.common.base.Preconditions;

/**
 * @author Limeth
 */
public class EmoticonPackEntry
{
	private final EmoticonPack pack;
	private final Emoticon emoticon;

	public EmoticonPackEntry(EmoticonPack pack, Emoticon emoticon)
	{
		Preconditions.checkNotNull(pack);
		Preconditions.checkNotNull(emoticon);

		this.pack = pack;
		this.emoticon = emoticon;
	}

	public EmoticonPack getPack()
	{
		return pack;
	}

	public Emoticon getEmoticon()
	{
		return emoticon;
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;
		if(o == null || getClass() != o.getClass())
			return false;

		EmoticonPackEntry that = (EmoticonPackEntry) o;

		if(!emoticon.equals(that.emoticon))
			return false;
		if(!pack.equals(that.pack))
			return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		int result = pack != null ? pack.hashCode() : 0;
		result = 31 * result + (emoticon != null ? emoticon.hashCode() : 0);
		return result;
	}

	@Override
	public String toString()
	{
		return "EmoticonPackEntry{" +
		       "pack=" + pack +
		       ", emoticon=" + emoticon +
		       '}';
	}
}
