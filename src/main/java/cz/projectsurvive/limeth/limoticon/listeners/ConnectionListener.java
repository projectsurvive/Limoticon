package cz.projectsurvive.limeth.limoticon.listeners;

import cz.projectsurvive.limeth.limoticon.LPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 * @author Limeth
 */
public class ConnectionListener implements Listener
{
	@EventHandler
	public void onPlayerJoin(PlayerJoinEvent event)
	{
		Player player = event.getPlayer();

		LPlayer.loadAndRegister(player);
	}

	@EventHandler
	public void onPlayerQuit(PlayerQuitEvent event)
	{
		Player player = event.getPlayer();

		LPlayer.saveAndUnregister(player);
	}
}
