package cz.projectsurvive.limeth.limoticon;

import com.google.common.collect.Lists;

import java.util.List;
import java.util.regex.Pattern;

/**
 * @author Limeth
 */
public class Emoticon
{
	private final String id;
	private final List<Pattern> aliases = Lists.newArrayList();

	public Emoticon(String id)
	{
		this.id = id;
	}

	public String getId()
	{
		return id;
	}

	public boolean addAlias(Pattern alias)
	{
		if(alias == null)
			throw new IllegalArgumentException("The alias must not be null.");

		if(hasAlias(alias))
			return false;

		aliases.add(alias);
		return true;
	}

	public boolean hasAlias(Pattern alias)
	{
		for(Pattern currentAlias : aliases)
			if(currentAlias.pattern().equals(alias.pattern()))
				return true;

		return false;
	}

	public List<Pattern> getAliases()
	{
		return Lists.newArrayList(aliases);
	}

	@Override
	public String toString()
	{
		return "Emoticon{" +
		       "id='" + id + '\'' +
		       ", aliases=" + aliases +
		       '}';
	}

	@Override
	public boolean equals(Object o)
	{
		if(this == o)
			return true;
		if(o == null || getClass() != o.getClass())
			return false;

		Emoticon emoticon = (Emoticon) o;

		if(!aliases.equals(emoticon.aliases))
			return false;
		if(!id.equals(emoticon.id))
			return false;

		return true;
	}

	@Override
	public int hashCode()
	{
		int result = id.hashCode();
		result = 31 * result + aliases.hashCode();
		return result;
	}

	public static class Replacement
	{
		private final String result;
		private final String replacedString;

		private Replacement(String result, String replacedString)
		{
			this.result = result;
			this.replacedString = replacedString;
		}

		public String getResult()
		{
			return result;
		}

		public String getReplacedString()
		{
			return replacedString;
		}
	}
}
