package cz.projectsurvive.limeth.limoticon;

import cz.projectsurvive.me.limeth.rawchatmanager.ChatArgumentHandler;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.List;
import java.util.Optional;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Limeth
 */
public class EmoticonChatHandler extends ChatArgumentHandler
{
	private static final String GROUP_REPLACE   = "replace";
	private static final String EMOTICON_PREFIX = ChatColor.WHITE.toString();
	private static final String EMOTICON_SUFFIX = ChatColor.RESET.toString();

	public EmoticonChatHandler()
	{
		super("limoticon", "limoticons");
	}

	@Override
	public String handle(Player sender, Player receiver, String originalMessage, String argument)
	{
		return replace(sender, argument);
	}

	private static String replace(Player player, String argument)
	{
		Limoticon limoticon = Limoticon.getInstance();
		EmoticonLibrary library = limoticon.getLibrary();
		LPlayer lPlayer = LPlayer.get(player);
		int aliasIndex = 0;
		int maxAliases = 0;

		do
		{
			for(EmoticonPack emoticonPack : lPlayer.getEmoticonPacks())
				for(Emoticon emoticon : library.getEmoticons(emoticonPack))
				{
					List<Pattern> aliases = emoticon.getAliases();

					if(aliasIndex == 0 && aliases.size() > maxAliases)
						maxAliases = aliases.size();
					else if(aliasIndex >= aliases.size())
						continue;

					Pattern alias = aliases.get(aliasIndex);
					argument = replace(alias, argument, emoticonPack, emoticon);
				}
		}
		while(++aliasIndex < maxAliases);

		return argument;
	}

	private static String replace(Pattern alias, String argument, EmoticonPack emoticonPack, Emoticon emoticon)
	{
		Optional<Character> character = Limoticon.getInstance().getLibrary().getCharacter(emoticonPack, emoticon);

		if(!character.isPresent())
			return argument;

		String characterString = EMOTICON_PREFIX + Character.toString(character.get()) + EMOTICON_SUFFIX;

		do
		{
			Matcher matcher = alias.matcher(argument);

			if(!matcher.find())
				break;

			try
			{
				matcher.group(GROUP_REPLACE);
			}
			catch(IllegalArgumentException e)
			{
				argument = matcher.replaceFirst(characterString);
				continue;
			}

			int replacedStringStart = matcher.start(GROUP_REPLACE);
			int replacedStringEnd = matcher.end(GROUP_REPLACE);
			argument = argument.substring(0, replacedStringStart) + characterString + argument.substring(replacedStringEnd);
		}
		while(true);

		return argument;
	}
}
